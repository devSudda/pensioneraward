package com.revision.DaoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.revision.data.dao.BaseDao;
import com.revision.interfaces.AwardDao;
import com.revision.responce.model.PensionerAward;

public class AwardPensionerImpl extends BaseDao implements AwardDao {

	Connection conn = getPensionerAwardConn();

	@Override
	public PensionerAward getAward(String nic, long filenum, String soldernum) {

		PensionerAward pA = new PensionerAward();

		String s1 = null, s2 = null, s3 = null, s4 = null, s5 = null, s6 = null, s7 = null, s8 = null, s9 = null,
				s10 = null;

		List<String> statusList = new ArrayList<String>();

		String ds1 = null, ds2 = null, ds3 = null, ds4 = null, ds5 = null, ds6 = null, ds7 = null, ds8 = null,
				ds9 = null, ds10 = null, ds11 = null, ds12 = null, ds13 = null, ds14 = null, ds15 = null;

		List<String> dateList = new ArrayList<String>();

		String sql = "SELECT id,receiveddate,filenumber,forcenum,nic,step1,step2,step3,step4,step5,step6,step7,"
				+ "step8,step9,step10,step11,step12,step13,step14, "
				+ "step15,step1date,step2date,step3date,step4date,step5date,step6date,step7date,"
				+ "step8date,step9date,step10date,step11date,step12date,step13date,step14date,"
				+ "step15date,name FROM register WHERE (nic=? OR filenumber=? OR forcenum =?)";

		try {

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, nic);
			ps.setLong(2, filenum);
			ps.setString(3, soldernum);
			System.out.println(ps);
			ResultSet rSet1 = ps.executeQuery();

			if (rSet1.next()) {
				pA.setDate(rSet1.getString(2));
				pA.setSolderNum(rSet1.getString(4));
				pA.setNic(rSet1.getString(5));
				pA.setFileNum(rSet1.getString(3));
				pA.setName(rSet1.getString(36));
				s1 = rSet1.getString(6);
				s2 = rSet1.getString(7);
				s3 = rSet1.getString(8);
				s4 = rSet1.getString(9);
				s5 = rSet1.getString(10);
				s6 = rSet1.getString(11);
				s7 = rSet1.getString(12);
				s8 = rSet1.getString(13);
				s9 = rSet1.getString(14);
				s10 = rSet1.getString(15);

				ds1 = rSet1.getString(16);
				ds2 = rSet1.getString(17);
				ds3 = rSet1.getString(18);
				ds4 = rSet1.getString(19);
				ds5 = rSet1.getString(20);
				ds6 = rSet1.getString(21);
				ds7 = rSet1.getString(22);
				ds8 = rSet1.getString(23);
				ds9 = rSet1.getString(24);
				ds10 = rSet1.getString(25);
				ds11 = rSet1.getString(26);
				ds12 = rSet1.getString(27);
				ds13 = rSet1.getString(28);
				ds14 = rSet1.getString(29);
				ds15 = rSet1.getString(30);

			}

			statusList.add(s1);
			statusList.add(s2);
			statusList.add(s3);
			statusList.add(s4);
			statusList.add(s5);
			statusList.add(s6);
			statusList.add(s7);
			statusList.add(s8);
			statusList.add(s9);
			statusList.add(s10);

			dateList.add(ds1);
			dateList.add(ds2);
			dateList.add(ds3);
			dateList.add(ds4);
			dateList.add(ds5);
			dateList.add(ds6);
			dateList.add(ds7);
			dateList.add(ds8);
			dateList.add(ds9);
			dateList.add(ds10);
			dateList.add(ds11);
			dateList.add(ds12);
			dateList.add(ds13);
			dateList.add(ds14);
			dateList.add(ds15);

			for (int i = 0; i < statusList.size(); i++) {
				if (statusList.get(i) != null) {
					int arrayPos = i;
					pA.setStatus("Status - " + statusList.get(arrayPos));
					System.out.println("Status " + pA.getStatus());
				}
			}

			for (int i = 0; i < dateList.size(); i++) {
				if (dateList.get(i) != null) {
					int arraypos1 = i;
					pA.setDate("DATE - " + dateList.get(arraypos1));
					System.out.println("Date" + pA.getDate());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			closeConnection(conn);
		}
		return pA;
	}

}
