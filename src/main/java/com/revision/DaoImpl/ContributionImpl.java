package com.revision.DaoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.revision.data.dao.BaseDao;
import com.revision.interfaces.ContributionDAO;
import com.revision.model.ContributionList;
import com.revision.responce.model.ReceiptSummaryListModel;

public class ContributionImpl extends BaseDao implements ContributionDAO{

	Connection conn=get145ConnectionPspf();
	
	@Override
	public ContributionList getContribution(int id) {
		ContributionList conList=new ContributionList();
		
		
		return conList;
	}

	@Override
	public ReceiptSummaryListModel getReceiptSum(int month, int year) {
		// TODO Auto-generated method stub
		
		ReceiptSummaryListModel rsl=new ReceiptSummaryListModel();
		String sql="SELECT pspf_service.pensionpoint_id, COUNT(pspf_cheque.id) AS ChCount, "
				+ "SUM(pspf_cheque.amount) AS amount "
				+ "FROM `pspf_cheque`,pspf_service, pspf_contributions "
				+ "WHERE pspf_service.id=pspf_contributions.service_id "
				+ "AND pspf_cheque.id=pspf_contributions.cheque_id "
				+ "AND pspf_cheque.cheque_date LIKE ? "
				+ "GROUP BY pspf_service.pensionpoint_id";
		try (PreparedStatement ps=conn.prepareStatement(sql)){
			
			ps.setString(1, "%"+year+"-"+month+"%");
				System.out.println(ps);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally {
			closeConnection(conn);
		}
		return null;
	}

	
}
