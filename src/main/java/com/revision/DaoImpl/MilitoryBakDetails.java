package com.revision.DaoImpl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.revision.data.dao.BaseDao;
import com.revision.interfaces.MilitoryDao;
import com.revision.responce.model.MilitoryBankData;



public class MilitoryBakDetails extends BaseDao implements MilitoryDao {

	@Override
	public MilitoryBankData getdata(String nic) {

		Connection con = get141Connection();
		
		String sql = "SELECT pen.BNNO, pen.BRNO, ba.BNAME, br.BRNAME, pen.BAC " + 
											 "FROM `pension` pen, `bank` ba, `branch` br " + 
												"WHERE pen.BNNO=ba.BNNO AND pen.BRNO=br.BRNO AND pen.BNNO=br.BNNO AND pen.PT IN (40,44,21,22) AND pen.NICNO=?";
		MilitoryBankData mdata = new MilitoryBankData();
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, nic);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				mdata =  new MilitoryBankData();
				
				if(rs.getString(1)== "1000") {
					mdata.setAccNu("000");
				}
				else {
					mdata.setAccNu(rs.getString(5));
				}
				mdata.setBankID(rs.getString(1));
				mdata.setBranch(rs.getString(4));
				mdata.setBankname(rs.getString(3));
				mdata.setBranchID(rs.getString(2));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mdata;
	}



	

	
	
	

}
