package com.revision.DaoImpl;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.revision.data.dao.BaseDao;
import com.revision.interfaces.PspfMemberDAO;
import com.revision.model.BasicPersonalInfoModel;
import com.revision.model.ContributionDetails;
import com.revision.model.PspfMemberModel;
import com.revision.responce.model.ContributionResModel;

public class PspfMemberImpl extends BaseDao implements PspfMemberDAO{
	
	Connection connPspf=get145ConnectionPspf();
	Connection conn=get145ConnectionLive();
	
	//testing-----------
	public List<String> getNuumbers(){
//		Connection conn=get141Connection();
		String[] nic=null;
		List<String> numbers=new ArrayList<String>();
		try {
			String text= new String(Files.readAllBytes(Paths.get("D:\\Pensio Project\\War_files\\Pspf_api\\numbers.txt")));
//			nic=text.split(",");
			System.out.println(text);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		for(int i=0; i<nic.length; i++) {
//			System.out.println(nic[i]);
		}
		return numbers;
	}
	//testing ends
	
	@Override
	public List<PspfMemberModel> getMember(int id) {
//		Connection conn=get145ConnectionPspf();
		List<PspfMemberModel> memberList=new ArrayList<PspfMemberModel>();
		String sql="SELECT pspf_member.id, pspf_member.dob, pspf_member.gender, pspf_member.nic, "
				+ "pspf_member.salutation, pspf_member.name, pspf_member.address_1, pspf_member.address_2, "
				+ "pspf_member.address_3, pspf_member.mobile, pspf_member.time_stamp "
				+ "FROM pspf_member "
				+ "WHERE pspf_member.id>?";
		try (PreparedStatement ps=connPspf.prepareStatement(sql);){
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet sr=ps.executeQuery();
			while(sr.next()) {
				PspfMemberModel pmm=new PspfMemberModel();
				pmm.setDob(sr.getString(2));
				pmm.setFullName(sr.getString(5)+"."+sr.getString(6));
				pmm.setGender(sr.getString(3));
				pmm.setId(sr.getInt(1));
				pmm.setName(sr.getString(6));
				pmm.setNic(sr.getString(4));
				pmm.setSalutation(sr.getString(5));
				pmm.setAddress1(sr.getString(7));
				pmm.setAddress2(sr.getString(8));
				pmm.setAddress3(sr.getString(9));
				pmm.setMobile(sr.getString(10));
				pmm.setPensionPoint(getPensionPoint(sr.getInt(1),"point"));
				pmm.setPensionPointId(Integer.parseInt(getPensionPoint(sr.getInt(1),"id")));
				
				String datee=sr.getString(11);
				String outPut=datee.substring(0,10);
				pmm.setTimeStamp(outPut+"");
				memberList.add(pmm);
			}
			sr.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally {
			closeConnection(connPspf);
		}
		return memberList;
	}
	
	private String getPensionPoint(int id, String type) {
		String pp="-";
		
		int pointId=0;
		String sqlPoint="SELECT pensionpoint_id FROM pspf_service WHERE member_id=?";
		try (PreparedStatement ps1=connPspf.prepareStatement(sqlPoint)){
			ps1.setInt(1, id);
			ResultSet rs1=ps1.executeQuery();
			if(rs1.next()) {
				pointId=rs1.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		String sql="SELECT point_name FROM pension_point WHERE point_id=?";
		try (PreparedStatement ps=conn.prepareStatement(sql)){
			ps.setInt(1, pointId);
			ResultSet rs=ps.executeQuery();
			if(rs.next() && pointId!=0) {
				pp=rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
//		finally {
//			closeConnection(conn);
//			closeConnection(connPspf);
//		}
		if(type.equals("id")) {
			return pointId+"";
		}else {

			return pp;	
		}
	}
	
	

	@Override
	public PspfMemberModel getMemberByNic(String nic, int pspf) {
//		Connection conn=get145ConnectionPspf();
		PspfMemberModel pmm=new PspfMemberModel();
		String sql;
		if(nic.equals("0") || pspf==0) {
			sql="SELECT pspf_member.id, pspf_member.dob, pspf_member.gender, pspf_member.nic, "
					+ "pspf_member.salutation, pspf_member.name, pspf_member.address_1, pspf_member.address_2, "
					+ "pspf_member.address_3, pspf_member.mobile, pspf_member.current_service "
					+ "FROM pspf_member "
					+ "WHERE (pspf_member.nic=? OR pspf_member.id=?)";
		}else {
			sql="SELECT pspf_member.id, pspf_member.dob, pspf_member.gender, pspf_member.nic, "
					+ "pspf_member.salutation, pspf_member.name, pspf_member.address_1, pspf_member.address_2, "
					+ "pspf_member.address_3, pspf_member.mobile, pspf_member.current_service "
					+ "FROM pspf_member "
					+ "WHERE (pspf_member.nic=? AND pspf_member.id=?)";
		}
		try (PreparedStatement ps=connPspf.prepareStatement(sql);){
			
			ps.setString(1, nic);
			ps.setInt(2, pspf);
			System.out.println(ps);
			ResultSet sr=ps.executeQuery();
			while(sr.next()) {
				
				pmm.setDob(sr.getString(2));
				pmm.setFullName(sr.getString(5)+". "+sr.getString(6));
				pmm.setGender(sr.getString(3));
				pmm.setId(sr.getInt(1));
				pmm.setName(sr.getString(6));
				pmm.setNic(sr.getString(4));
				pmm.setSalutation(sr.getString(5));
				pmm.setAddress1(sr.getString(7));
				pmm.setAddress2(sr.getString(8));
				pmm.setAddress3(sr.getString(9));
				pmm.setMobile(sr.getString(10));
				pmm.setPensionPoint(getPensionPoint(sr.getInt(1),"point"));
				pmm.setPensionPointId(Integer.parseInt(getPensionPoint(sr.getInt(1),"id")));
			}
			sr.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally {
			closeConnection(connPspf);
		}
		return pmm;
	}
	
	public ContributionResModel getConDetails(String nic) {
//		Connection connPspf1=get145ConnectionPspf();
		List<ContributionDetails> cList=new ArrayList<ContributionDetails>();
		ContributionResModel crm=new ContributionResModel();
		String sql="SELECT pspf_member.`id`, pspf_member.`nic`, "
				+ "pspf_member.`name`, pspf_member.`salutation`, "
				+ "pspf_service.designation, pspf_service.institute, "  //6
				+ "pspf_contributions.year, pspf_contributions.payment_type, " //8
				+ "pspf_contributions.startmonth, pspf_contributions.endmonth, " //10
				+ "pspf_contributions.contribution_8,pspf_contributions.contribution_12 " + //12
				"FROM `pspf_member`, pspf_service, pspf_contributions " + 
				"WHERE pspf_member.id=pspf_service.member_id " + 
				"AND pspf_contributions.service_id=pspf_service.id " + 
				"AND pspf_member.nic=?";
		try (PreparedStatement ps=connPspf.prepareStatement(sql);){
			
			ps.setString(1, nic);
			
			ResultSet rs=ps.executeQuery();
			BasicPersonalInfoModel bpi=new BasicPersonalInfoModel();
			while(rs.next()) {
				
				
				bpi.setPspfNo(rs.getInt(1));
				bpi.setNic(rs.getString(2));
				bpi.setName(rs.getString(4)+"."+rs.getString(3));
				bpi.setDesignation(rs.getString(5));
				bpi.setCompany(rs.getString(6));
				
				ContributionDetails cd=new ContributionDetails();
				cd.setYear(rs.getInt(7));
				cd.setType(rs.getString(8));
				cd.setStartMonth(rs.getInt(9));
				cd.setEndMonth(rs.getInt(10));
				cd.setCon_8(rs.getDouble(11));
				cd.setCon_12(rs.getDouble(12));
				cList.add(cd);
			}
			crm.setPersonal(bpi);
			crm.setConDetails(cList);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally {
			closeConnection(connPspf);
		}
		return crm;
	}

}
