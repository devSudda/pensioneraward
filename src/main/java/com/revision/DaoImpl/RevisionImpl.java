package com.revision.DaoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import com.revision.data.dao.BaseDao;
import com.revision.interfaces.RevisionDAO;
import com.revision.model.BasicDataDTO;
import com.revision.model.RevisionPensionerList;
import com.revision.model.RevisionPensionerModel;
import com.revision.responce.model.BasicDataList;

public class RevisionImpl extends BaseDao implements RevisionDAO{

//	Connection conn147=get141Connection();
	Connection conn145=get145ConnectionLive();
	Connection conn146Civil=get146CivilConnection();
	Connection conn146Manual=get146ManualConnection();
	Connection conn146Forces=get146ForcesConnection();
	Connection conn141=get141Connection();
	@Override
	public RevisionPensionerList getRevisionList(String nic, int penno, int pt) {
		// TODO Auto-generated method stub
		String typeString="";
		int[] civilTypes= {1,2,15};
		List<int[]> civilList = new ArrayList<>(Arrays.asList(civilTypes));
		if(civilList.contains(pt)) {
			typeString="civil";
		}else if(pt==3) {
			typeString="milcom,milnoncom";
		}else {}
		RevisionPensionerList rpl=new RevisionPensionerList();
		List<RevisionPensionerModel> rpmList=new ArrayList<RevisionPensionerModel>();
		String sql145="SELECT pension.salary_scale, regular_pensioner.grade, pensioner.retired_date, pension.consolidated_salary "
				+ "FROM pension, pensioner, regular_pensioner "
				+ "WHERE pension.pensioner_id=pensioner.pensioner_id AND regular_pensioner.pensioner_id=pension.pensioner_id "
				+ "AND pensioner.nic=? AND pension.pension_id=?";
		
		String sql146rev="SELECT revision.salary_scale, pension_service.service_grade, pension_service.retired_date, pension.basic_salary "
				+ "FROM revision, pension_service, pension, person "
				+ "WHERE person.id=revision.person_id AND pension_service.person_id=person.id "
				+ "AND pension.person_id=person.id AND person.nic LIKE ? AND revision.pension_id=?";
		
		
		
		try {
			//FROM 145
			PreparedStatement ps=conn145.prepareStatement(sql145);
			ps.setString(1, nic);
			ps.setInt(2, penno);
			
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()) {
				RevisionPensionerModel rpm=new RevisionPensionerModel();
				rpm.setSalaryScale(rs.getString(1));
				rpm.setGrade(rs.getString(2));
				String retDate=rs.getString(3);
				rpm.setRetYear(Integer.parseInt(retDate.substring(0, 4)));
				rpm.setBasicSalary(Double.parseDouble(
						String.format("%.2f", 
								rs.getDouble(4)/12)));
				rpmList.add(rpm);
				ps.close();
				rs.close();
			}else {
				RevisionPensionerModel rpmRev=new RevisionPensionerModel();
				//FROM 146 CIVIL
				PreparedStatement ps146Civil=conn146Civil.prepareStatement(sql146rev);
				ps146Civil.setString(1, "%"+nic+"%");
				ps146Civil.setInt(2, penno);
				System.out.println(ps146Civil);
				ResultSet rsCivil=ps146Civil.executeQuery();
				
				if(rsCivil.next()) {
					rpmRev.setSalaryScale(rsCivil.getString(1));
					rpmRev.setGrade(rsCivil.getString(2));
					String retDate=rsCivil.getString(3);
					rpmRev.setRetYear(Integer.parseInt(retDate.substring(0, 4)));
					rpmRev.setBasicSalary(Double.parseDouble(
							String.format("%.2f", 
									rsCivil.getDouble(4)/12)));
					rpmList.add(rpmRev);
					ps146Civil.close();
					rsCivil.close();
				}else {
					//FROM 146 FORCES
					PreparedStatement ps146Forces=conn146Forces.prepareStatement(sql146rev);
					ps146Forces.setString(1, nic);
					ps146Forces.setInt(2, penno);
					ResultSet rsForces=ps146Forces.executeQuery();
					
					if(rsForces.next()) {
						rpmRev.setSalaryScale(rsForces.getString(1));
						rpmRev.setGrade(rsForces.getString(2));
						rpmRev.setRetYear(rsForces.getInt(3));
						rpmRev.setBasicSalary(Double.parseDouble(
								String.format("%.2f", 
										rsForces.getDouble(4)/12)));
						rpmList.add(rpmRev);
						ps146Forces.close();
						rsForces.close();
					}else {
						//FROM 146 MANUAL
						PreparedStatement ps146Manual=conn146Manual.prepareStatement(sql146rev);
						ps146Manual.setString(1, nic);
						ps146Manual.setInt(2, penno);
						ResultSet rsManual=ps146Manual.executeQuery();
						
						if(rsManual.next()) {
							rpmRev.setSalaryScale(rsManual.getString(1));
							rpmRev.setGrade(rsManual.getString(2));
							rpmRev.setRetYear(rsManual.getInt(3));
							rpmRev.setBasicSalary(Double.parseDouble(
									String.format("%.2f", 
											rsManual.getDouble(4)/12)));
							rpmList.add(rpmRev);
							ps146Manual.close();
							rsManual.close();
						}
					}	
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
//		int getFrom145=checkRevision(nic, penno, pt, "145");
//		int getFrom146Civil=checkRevision(nic, penno, pt, "146Civil");
//		int getFrom146Manual=checkRevision(nic, penno, pt, "146Manual");
//		int getFrom146Forces=checkRevision(nic, penno, pt, "146Froces");
		
		rpl.setPenList(rpmList);
		
		return rpl;
	}
	
	private int checkRevision(String nic, int penno, int pt, String db) {
		String sql="";
		int result=0;
		if(db.equals("145")) {
			sql="SELECT COUNT(pension.pensioner_id) AS c "
					+ "FROM pension, pensioner "
					+ "WHERE pension.pensioner_id=pensioner.pensioner_id AND pensioner.nic=?";
			try (PreparedStatement ps=conn145.prepareStatement(sql)){
				ps.setString(1, nic);
				ResultSet rs=ps.executeQuery();
				if(rs.next()) {
					result=rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}
		
		return result;
	}

	@Override
	public BasicDataList getBasicData(String nic, int penno) {
		// TODO Auto-generated method stub
		BasicDataList bdl=new BasicDataList();
		String queryPart="";
		if(nic.equals("0")&&penno!=0) {
			queryPart="pension.PENNO=?";
		}else if(!nic.equals("0") && penno==0) {
			queryPart="pension.NICNO=?";
		}else if(!nic.equals("0") && penno!=0) {
			queryPart="pension.NICNO=? AND pension.PENNO=?";
		}
		String sql="SELECT typese.TYPE, pension.PENNO, pension.NAME, pension.AD1, pension.AD2, pension.AD3, pension.AD4, pension.NICNO, " //8
				+ "pension.MOBILE, CASE WHEN pension.DOB!='0000-00-00' THEN pension.DOB END new_date, " //10
				+ "pension.WOPNO, pension.DSCODE, " //12
				+ "pension.LIFE_CERTIFICATE_RECEIVED_DATE, pension.BRNO, status.SNAME, pension.ID, pension.BAC, " //17
				+ "pension.BNNO, gncode_new.GNCODE, gncode_new.DS_NAME, pension.BPEN, pension.OTA, pension.CLA, pension.TPEN, pension.PT " //25
				+ "FROM pension, gncode_new, typese, status "
				+ "WHERE status.ACTIVE=pension.ACTIVE AND typese.PT=pension.PT AND pension.GSNO=gncode_new.GNCODE "
				+ "AND "+queryPart+" GROUP BY pension.NICNO";
		try (PreparedStatement ps=conn141.prepareStatement(sql)){
			if(nic.equals("0")&&penno!=0) {
				ps.setInt(1, penno);
			}else if(!nic.equals("0") && penno==0) {
				ps.setString(1, nic);
			}else if(!nic.equals("0") && penno!=0) {
				ps.setString(1, nic);
				ps.setInt(2, penno);
			}
			System.out.println(ps);
			ResultSet sr=ps.executeQuery();
			List<BasicDataDTO> basicList=new ArrayList<BasicDataDTO>();
			while(sr.next()) {
				BasicDataDTO bdd=new BasicDataDTO();
				bdd.setAddress(sr.getString(4)+","+sr.getString(5)+","+sr.getString(6));
				bdd.setBpen(sr.getDouble(21));
				bdd.setCla(sr.getDouble(23));
				bdd.setContactNo(sr.getString(9));
				bdd.setDob(sr.getString(10));
				bdd.setDod("test");
				bdd.setFullName(sr.getString(3));
				bdd.setGender("test");
				bdd.setNic(sr.getString(8));
				bdd.setOta(sr.getDouble(22));
				bdd.setPenNo(sr.getLong(2));
				bdd.setPenType(sr.getString(1));
				bdd.setPersonStatus(sr.getString(15));
				bdd.setTotalPen(sr.getDouble(24));
				bdd.setPenTypeId(sr.getInt(25));
				bdd.setWnop(sr.getString(11));
				basicList.add(bdd);
			}
			bdl.setDataList(basicList);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally {
			closeConnection(conn141);
		}
		return bdl;
	}
	

}
