package com.revision.consts;

public class ApiConsts {
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static String MYSQL_JDBC_DRIVER_141 = "org.mariadb.jdbc.Driver";

	// public static final String DB_URL =
	// "jdbc:mysql://192.168.100.199:3000/master";

	public static final String DB_URL = "jdbc:mysql://192.168.100.145:3306/master"; // Test
//	public static final String DB_URL = "jdbc:mysql://192.168.204.193:3306/master"; //RedHat

	public static final String DB_URL_145 = "jdbc:mysql://192.168.100.150:3306/pspf"; // Test
//	public static final String DB_URL_145 = "jdbc:mysql://192.168.204.193:3306/pspf"; //RedHat

	public static final String DB_URL_145_Live = "jdbc:mysql://192.168.100.150:3306/live"; // Test
//	public static final String DB_URL_145_Live = "jdbc:mysql://192.168.204.193:3306/live"; //RedHat

	public static final String DB_URL_147 = "jdbc:mysql://192.168.100.150:3306/pms_test"; // testing PMS3
//	public static final String DB_URL_147 = "jdbc:mysql://192.168.100.147:3306/pms3";      //live    PMS3
//	public static final String DB_URL_147 = "jdbc:mysql://192.168.204.220:3306/pms3";      //RedHat

//	public static final String DB_URL_145 = "jdbc:mysql://192.168.100.150:3306/pensions";
//	public static final String DB_URL_141 = "jdbc:mysql://192.168.100.141/pensions";     //live
	public static final String DB_URL_141 = "jdbc:mysql://192.168.100.150:3306/pensions"; // test
//	public static final String DB_URL_141 = "jdbc:mysql://192.168.204.117:3306/pensions";   //RedHat

	public static final String DB_URL_146_civil = "jdbc:mysql://192.168.100.150:3306/pms_revision"; // Test
//	public static final String DB_URL_146_civil = "jdbc:mysql://192.168.204.214:3306/pms_revision"; //RedHat

	public static final String DB_URL_146_manual = "jdbc:mysql://192.168.100.150:3306/pms_revision_manual"; // Test
//	public static final String DB_URL_146_manual = "jdbc:mysql://192.168.204.214:3306/pms_revision_manual"; //RedHat

	public static final String DB_URL_146_forces = "jdbc:mysql://192.168.100.150:3306/pms_revision_forces"; // Test
//	public static final String DB_URL_146_forces = "jdbc:mysql://192.168.204.214:3306/pms_revision_forces"; //RedHat


//	public static final String DB_URL_Pen_Award = "jdbc:mysql://192.168.204.193:3306/awards10"; //RedHat
	public static final String DB_URL_Pen_Award = "jdbc:mysql://192.168.100.150:3306/awards10"; // test

	public static final String USER = "sasini"; // Test
	public static final String PASS = "sasini"; // Test
//	public static final String USER = "develop"; //RedHat
//	public static final String PASS = "Root@d0p"; //RedHat

	public static final String USER147 = "sasini";       //test
	public static final String PASS147 = "sasini"; //test
//	public static final String USER147 = "develop"; // RedHat
//	public static final String PASS147 = "Root@d0p"; // RedHat

	public static final String USER146 = "sasini";  //test
	public static final String PASS146 = "sasini"; //test
//	public static final String USER146 = "develop"; // RedHat
//	public static final String PASS146 = "Root@d0p"; // RedHat

//	public static final String USER141 = "itprojectdev";  //Live
//    public static final String PASS141 = "@8Sa#g=R";     //Live
//	public static final String USER141 = "develop"; // RedHat
//	public static final String PASS141 = "Root@d0p"; // RedHat
	public static final String USER141 = "sasini";
	public static final String PASS141 = "sasini";
}
