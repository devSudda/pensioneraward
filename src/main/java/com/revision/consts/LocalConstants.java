package com.revision.consts;

public class LocalConstants {
	/**
	 * for JWT token0
	 */
	// public final static String SECRET =
	// "8803450027432909480347211003443503478153599493337712676282516550107736252572396456276431396991767820";
	public final static String SECRET = "0a6b944d-d2fb-46fc-a85e-0295c986cd9f";
	/*
	 * public final static String USERNAME = "1"; public static final String USER_ID
	 * = "2"; public final static String ROLE = "3"; public static final String
	 * PENSION_POINT = "4"; public final static String SUBJECT = "users/auth";
	 */

	public static final String ROLE = "a";
	public static final String USERNAME = "b";
	public static final String TOKEN_DATA = "Token-Data";
	public static final String PENSION_POINT = "d";

	/**
	 * proc response returns
	 */
	public static final int SUCCESSFULL = 1;
	public static final int FAILED = -1;

}
