package com.revision.data;

import com.revision.DaoImpl.AwardPensionerImpl;
import com.revision.DaoImpl.ContributionImpl;
import com.revision.DaoImpl.MilitoryBakDetails;
import com.revision.DaoImpl.PspfMemberImpl;
import com.revision.DaoImpl.RevisionImpl;
import com.revision.interfaces.AwardDao;
import com.revision.interfaces.ContributionDAO;
import com.revision.interfaces.MilitoryDao;
import com.revision.interfaces.PspfMemberDAO;
import com.revision.interfaces.RevisionDAO;

public class DaoManager {

	public static PspfMemberDAO memberDao() {
		return new PspfMemberImpl();
	}

	public static ContributionDAO contributtionDao() {
		return new ContributionImpl();
	}

	public static RevisionDAO revisionDao() {
		return new RevisionImpl();
	}

	public static AwardDao awarddao() {
		return new AwardPensionerImpl();
	}
	public static MilitoryDao mldao()  {
		return new MilitoryBakDetails();
	}
}
