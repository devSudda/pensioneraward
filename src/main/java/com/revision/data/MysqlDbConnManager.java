package com.revision.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.revision.consts.ApiConsts;

public enum MysqlDbConnManager {

	MANAGER;

	public Connection getConnection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_141, ApiConsts.USER, ApiConsts.PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get145Connection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_145, ApiConsts.USER, ApiConsts.PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get145ConnectionLive() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_145_Live, ApiConsts.USER, ApiConsts.PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get141Connection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.MYSQL_JDBC_DRIVER_141);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_141, ApiConsts.USER141, ApiConsts.PASS141);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get145PensionsConnection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_145, ApiConsts.USER, ApiConsts.PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get146CivilConnection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_146_civil, ApiConsts.USER146, ApiConsts.PASS146);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get146ForcesConnection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_146_forces, ApiConsts.USER146, ApiConsts.PASS146);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection get146ManualConnection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_146_manual, ApiConsts.USER146, ApiConsts.PASS146);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;

	}

	public Connection get147Connection() {
		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_147, ApiConsts.USER147, ApiConsts.PASS147);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public Connection getPensionerAwardConn() {

		Connection dbConn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			dbConn = DriverManager.getConnection(ApiConsts.DB_URL_Pen_Award, ApiConsts.USER, ApiConsts.PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConn;
	}

	public void closeConnection(Connection dbConn) {
		try {
			dbConn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closePreparedStatement(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeResultSet(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
