package com.revision.data;

import java.io.UnsupportedEncodingException;
import java.util.Date;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.revision.consts.LocalConstants;
import com.revision.exceptions.TokenExpiredException;
import com.revision.exceptions.TokenGenerationException;
import com.revision.exceptions.TokenNotFoundException;
import com.revision.model.UserModel;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenManager {
	private Logger logger = LoggerFactory.getLogger(TokenManager.class);

	private static final String DELIGAN = "deligan";
	private static final String ENCODING = "UTF-8";
	
	public static final String ROLE = "a";
	public static final String USERNAME = "b";
	public static final String TOKEN_DATA = "Token-Data";
	public static final String PENSION_POINT = "d";

	public String generateToken(UserModel model) {

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		//Let's set the JWT Claims
		JwtBuilder builder = null;
		try {
			/*builder = Jwts.builder()
					.setIssuedAt(now)
					.setSubject(LocalConstants.SUBJECT)
					.setIssuer(DELIGAN)
					.claim(LocalConstants.USERNAME, model.getUsername())
					.claim(LocalConstants.USER_ID, model.getUserId())
					.signWith(SignatureAlgorithm.HS256, LocalConstants.SECRET.getBytes(ENCODING));
			*/
			builder = Jwts.builder()
					.setIssuedAt(now)
					.setIssuer(DELIGAN)
					.claim(LocalConstants.USERNAME, model.getUsername())
					.signWith(SignatureAlgorithm.HS256, LocalConstants.SECRET.getBytes(ENCODING));

		} catch (UnsupportedEncodingException e) {
			throw new TokenGenerationException("failed to generate the auth token");  
		}

		//if it has been specified, let's add the expiration
		long expMillis = nowMillis + 3600000 * 8;
		Date exp = new Date(expMillis);
		builder.setExpiration(exp);

		//Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	public Claims validateToken(String token) {
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(LocalConstants.SECRET.getBytes(ENCODING))
					.parseClaimsJws(token).getBody();

		} catch (ExpiredJwtException e) {
			logger.info("token expired");
			throw new TokenExpiredException();
			
		} catch (Exception e) {
			System.out.println("invalid token");
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new TokenNotFoundException();
		}

		return claims;
	}
	
	/**
	 * 
	 * @param token
	 * @return
	 */
	public UserModel isAuthorized(String token) {
		logger.info("tring to authorize the user");
		UserModel data = null;
		TokenManager tokenManager = new TokenManager();
		Claims claim = null;
          System.out.println(token);
		if (token != null) {
			claim = tokenManager.validateToken(token);
		} else {
			logger.info("null token received");
		}

		if (claim != null) {
			String role = (String) claim.get(LocalConstants.ROLE);
			int pensionPointId = (int) claim.get(LocalConstants.PENSION_POINT);
			String username = (String) claim.get(LocalConstants.USERNAME);

			data = new UserModel();
			data.setPensionPoint(pensionPointId);
			data.setRole(role);
			data.setUsername(username);

			logger.info("Valid token");
		}

		logger.info("user authorized, username (" + data.getUsername() + "), pension point (" + data.getPensionPoint()
				+ ")");
		return data;
	}
}
