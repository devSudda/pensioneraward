package com.revision.data.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.revision.data.MysqlDbConnManager;
import com.revision.model.PspfMemberModel;

public class BaseDao {
private Logger logger = LoggerFactory.getLogger(BaseDao.class);
	
	/*public Connection getConnection() {
		logger.info(" connection execute ");
		return MysqlDbConnManager.MANAGER.getConnection();
	}*/
	
	public Connection get145ConnectionPspf(){
		logger.info(" connection execute ");
		return MysqlDbConnManager.MANAGER.get145Connection();
	}
	
	public Connection get145ConnectionLive() {
		logger.info(" connection execute 145");
		return MysqlDbConnManager.MANAGER.get145ConnectionLive();
	}
	
	public Connection get147Connection() {
//		logger.info("147 connection execute ");
		return MysqlDbConnManager.MANAGER.get147Connection();
	}
	
	public Connection get141Connection() {
		logger.info("141 connection execute ");
		return MysqlDbConnManager.MANAGER.get141Connection();
	}
	
	public Connection get145ConnectionPensions() {
		logger.info("145 pensions connection execute ");
		return MysqlDbConnManager.MANAGER.get145PensionsConnection();
	}
	
	public Connection get146CivilConnection() {
		logger.info("146 connection execute...");
		return MysqlDbConnManager.MANAGER.get146CivilConnection();
	}
	public Connection get146ManualConnection() {
		logger.info("146 connection execute...");
		return MysqlDbConnManager.MANAGER.get146ManualConnection();
	}
	public Connection get146ForcesConnection() {
		logger.info("146 connection execute...");
		return MysqlDbConnManager.MANAGER.get146ForcesConnection();
	}
	
	public Connection getPensionerAwardConn() {
		logger.info("Pensioner Award Connection Exceute");
		return MysqlDbConnManager.MANAGER.getPensionerAwardConn();
	}
	
	public void closeConnection(Connection conn) {
		try {
			logger.info("Connection Closed...");
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<PspfMemberModel> getMember(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
