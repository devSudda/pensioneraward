package com.revision.exceptions;


/**
 * @author Shalitha Mihirnga
 *
 */
public class DataNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4337556377738999174L;

	public DataNotFoundException(String message) {
		super(message);
	}
	
}
