package com.revision.exceptions;

/**
 * @author Shalitha Mihirnga
 *
 */
public class InsertNotSuccessfulException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2264884029720211615L;

	public InsertNotSuccessfulException(String message) {
		super(message);
	}
	
	public InsertNotSuccessfulException() {
		super("Insert was not successful");
	}
	
}
