package com.revision.exceptions;


/**
 * @author Shalitha Mihirnga
 *
 */
public class MysqlException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MysqlException(Exception e) {
		super(e.getMessage());
	}
	
}
