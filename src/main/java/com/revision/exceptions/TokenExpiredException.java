package com.revision.exceptions;

import javax.xml.ws.WebServiceException;

public class TokenExpiredException extends WebServiceException{

private static final long serialVersionUID = 1L;
	
	public TokenExpiredException() {
		super("token expired");
	}
}
