package com.revision.exceptions;

import javax.xml.ws.WebServiceException;

public class TokenGenerationException extends WebServiceException{

	private static final long serialVersionUID = 1L;
	
	public TokenGenerationException(String message) {
		super(message);
	}
}
