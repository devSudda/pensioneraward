package com.revision.exceptions;

import javax.xml.ws.WebServiceException;

public class TokenNotFoundException extends WebServiceException{

private static final long serialVersionUID = 1L;
	
	public TokenNotFoundException() {
		super("authrization token not found or invalid token provided");
	}
}
