package com.revision.exceptions;


/**
 * @author Shalitha Mihirnga
 *
 */
public class UnAuthorizedException extends Exception {

	private static final long serialVersionUID = -3899851476049292084L;
	
	public UnAuthorizedException(String message) {
		super(message);
	}
	
	public UnAuthorizedException() {
		super("User is not authenticated");
	}
	
}
