package com.revision.exceptions;


/**
 * @author Shalitha Mihirnga
 *
 */
public class UserNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public UserNotFoundException(String message) {
		super(message);
	}
	
}
