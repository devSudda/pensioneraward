package com.revision.interfaces;

import com.revision.responce.model.PensionerAward;

public interface AwardDao {

	PensionerAward getAward(String nic, long filenum, String soldernum);
	
	
}
