package com.revision.interfaces;

import com.revision.model.ContributionList;
import com.revision.responce.model.ReceiptSummaryListModel;

public interface ContributionDAO {
	ContributionList getContribution(int id);
	ReceiptSummaryListModel getReceiptSum(int month, int year);
}
