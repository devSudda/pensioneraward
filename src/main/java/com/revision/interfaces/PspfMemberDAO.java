package com.revision.interfaces;

import java.util.List;

import com.revision.model.PspfMemberModel;

public interface PspfMemberDAO {
	List<PspfMemberModel> getMember(int id);
	PspfMemberModel getMemberByNic(String nic, int pspf);
}
