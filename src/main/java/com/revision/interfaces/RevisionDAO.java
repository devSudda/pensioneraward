package com.revision.interfaces;

import com.revision.model.RevisionPensionerList;
import com.revision.responce.model.BasicDataList;

public interface RevisionDAO {
	RevisionPensionerList getRevisionList(String nic, int penno, int pt);
	BasicDataList getBasicData(String nic, int penno);
}
