package com.revision.model;

public class BasicDataDTO {
	private long penNo;
	private String penType;
	private int penTypeId;
	private String fullName;
	private String nic;
	private String gender;
	private String address;
	private String contactNo;
	private String dob;
	private String dod;
	private String personStatus; //(active/dead)
	private String wnop;
	private double bpen;
	private double cla;
	private double ota;
	private double totalPen;
	private int dsCode;
	private String ds;
	
	

	
	
	public BasicDataDTO(long penNo, String penType, int penTypeId, String fullName, String nic, String gender,
			String address, String contactNo, String dob, String dod, String personStatus, String wnop, double bpen,
			double cla, double ota, double totalPen, int dsCode, String ds) {
		super();
		this.penNo = penNo;
		this.penType = penType;
		this.penTypeId = penTypeId;
		this.fullName = fullName;
		this.nic = nic;
		this.gender = gender;
		this.address = address;
		this.contactNo = contactNo;
		this.dob = dob;
		this.dod = dod;
		this.personStatus = personStatus;
		this.wnop = wnop;
		this.bpen = bpen;
		this.cla = cla;
		this.ota = ota;
		this.totalPen = totalPen;
		this.dsCode = dsCode;
		this.ds = ds;
	}



	public int getPenTypeId() {
		return penTypeId;
	}



	public void setPenTypeId(int penTypeId) {
		this.penTypeId = penTypeId;
	}



	public int getDsCode() {
		return dsCode;
	}



	public void setDsCode(int dsCode) {
		this.dsCode = dsCode;
	}



	public String getDs() {
		return ds;
	}



	public void setDs(String ds) {
		this.ds = ds;
	}



	public BasicDataDTO() {
		super();
	}

	public long getPenNo() {
		return penNo;
	}

	public void setPenNo(long penNo) {
		this.penNo = penNo;
	}

	public String getPenType() {
		return penType;
	}

	public void setPenType(String penType) {
		this.penType = penType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDod() {
		return dod;
	}

	public void setDod(String dod) {
		this.dod = dod;
	}

	public String getPersonStatus() {
		return personStatus;
	}

	public void setPersonStatus(String personStatus) {
		this.personStatus = personStatus;
	}

	public String getWnop() {
		return wnop;
	}

	public void setWnop(String wnop) {
		this.wnop = wnop;
	}

	public double getBpen() {
		return bpen;
	}

	public void setBpen(double bpen) {
		this.bpen = bpen;
	}

	public double getCla() {
		return cla;
	}

	public void setCla(double cla) {
		this.cla = cla;
	}

	public double getOta() {
		return ota;
	}

	public void setOta(double ota) {
		this.ota = ota;
	}

	public double getTotalPen() {
		return totalPen;
	}

	public void setTotalPen(double totalPen) {
		this.totalPen = totalPen;
	}
	
	
}
