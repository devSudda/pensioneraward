package com.revision.model;

public class BasicPersonalInfoModel {
	private int pspfNo;
	private String nic;
	private String name;
	private String designation;
	private String company;
	
	public BasicPersonalInfoModel(int pspfNo, String nic, String name, String designation, String company) {
		super();
		this.pspfNo = pspfNo;
		this.nic = nic;
		this.name = name;
		this.designation = designation;
		this.company = company;
	}

	public BasicPersonalInfoModel() {
		super();
	}

	public int getPspfNo() {
		return pspfNo;
	}

	public void setPspfNo(int pspfNo) {
		this.pspfNo = pspfNo;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	
}
