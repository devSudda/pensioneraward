package com.revision.model;

public class ContributionDetails {
	private int year;
	private String type;
	private int startMonth;
	private int endMonth;
	private double con_8;
	private double con_12;
	
	public ContributionDetails(int year, String type, int startMonth, int endMonth, double con_8, double con_12) {
		super();
		this.year = year;
		this.type = type;
		this.startMonth = startMonth;
		this.endMonth = endMonth;
		this.con_8 = con_8;
		this.con_12 = con_12;
	}

	public ContributionDetails() {
		super();
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(int startMonth) {
		this.startMonth = startMonth;
	}

	public int getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(int endMonth) {
		this.endMonth = endMonth;
	}

	public double getCon_8() {
		return con_8;
	}

	public void setCon_8(double con_8) {
		this.con_8 = con_8;
	}

	public double getCon_12() {
		return con_12;
	}

	public void setCon_12(double con_12) {
		this.con_12 = con_12;
	}
	
	
}
