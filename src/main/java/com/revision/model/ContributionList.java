package com.revision.model;

import java.util.List;

public class ContributionList {
	private List<ContributionModel> contributionList;

	public ContributionList(List<ContributionModel> contributionList) {
		super();
		this.contributionList = contributionList;
	}

	public ContributionList() {
		super();
	}

	public List<ContributionModel> getContributionList() {
		return contributionList;
	}

	public void setContributionList(List<ContributionModel> contributionList) {
		this.contributionList = contributionList;
	}
	
	
}
