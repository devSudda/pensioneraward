package com.revision.model;

public class ContributionModel {
	private int year;
	private String month;
	private double salary;
	
	public ContributionModel(int year, String month, double salary) {
		super();
		this.year = year;
		this.month = month;
		this.salary = salary;
	}

	public ContributionModel() {
		super();
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
}
