package com.revision.model;

public class PaymentModel {

	private String name;
	private int memberId;
	private int serviceId;
	private int month;
	private String monthString;
	private String paymentType;
	private double salary;
	private double salCon; // Salary Contribution
	private double depCon; // Department Contribution
	private double otherDeductions;
	private double otherAllowances;
	private double calculatedSalary;
	
	public PaymentModel(String name, int memberId, int serviceId, int month, String monthString, String paymentType,
			double salary, double salCon, double depCon, double otherDeductions, double otherAllowances,
			double calculatedSalary) {
		super();
		this.name = name;
		this.memberId = memberId;
		this.serviceId = serviceId;
		this.month = month;
		this.monthString = monthString;
		this.paymentType = paymentType;
		this.salary = salary;
		this.salCon = salCon;
		this.depCon = depCon;
		this.otherDeductions = otherDeductions;
		this.otherAllowances = otherAllowances;
		this.calculatedSalary = calculatedSalary;
	}

	public PaymentModel() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getMonthString() {
		return monthString;
	}

	public void setMonthString(String monthString) {
		this.monthString = monthString;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getSalCon() {
		return salCon;
	}

	public void setSalCon(double salCon) {
		this.salCon = salCon;
	}

	public double getDepCon() {
		return depCon;
	}

	public void setDepCon(double depCon) {
		this.depCon = depCon;
	}

	public double getOtherDeductions() {
		return otherDeductions;
	}

	public void setOtherDeductions(double otherDeductions) {
		this.otherDeductions = otherDeductions;
	}

	public double getOtherAllowances() {
		return otherAllowances;
	}

	public void setOtherAllowances(double otherAllowances) {
		this.otherAllowances = otherAllowances;
	}

	public double getCalculatedSalary() {
		return calculatedSalary;
	}

	public void setCalculatedSalary(double calculatedSalary) {
		this.calculatedSalary = calculatedSalary;
	}
	
	
}
