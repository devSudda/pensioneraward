package com.revision.model;

import java.util.ArrayList;
import java.util.List;

public class PspfMemberList {
	private List<PspfMemberModel> members=new ArrayList<>();

	public PspfMemberList(List<PspfMemberModel> members) {
		super();
		this.members = members;
	}

	public PspfMemberList() {
		super();
	}

	public List<PspfMemberModel> getMembers() {
		return members;
	}

	public void setMembers(List<PspfMemberModel> members) {
		this.members = members;
	}
 
	
	
}
