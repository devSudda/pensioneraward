package com.revision.model;

public class PspfMemberModel {
	
	private int id;
	private String dob;
	private String gender;
	private String salutation;
	private String name;
	private String fullName;
	private String nic;
	private String address1;
	private String address2;
	private String address3;
	private String mobile;
	private int pensionPointId;
	private String pensionPoint;
	private String timeStamp;
	
	

	public PspfMemberModel(int id, String dob, String gender, String salutation, String name, String fullName,
			String nic, String address1, String address2, String address3, String mobile, int pensionPointId,
			String pensionPoint, String timeStamp) {
		super();
		this.id = id;
		this.dob = dob;
		this.gender = gender;
		this.salutation = salutation;
		this.name = name;
		this.fullName = fullName;
		this.nic = nic;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.mobile = mobile;
		this.pensionPointId = pensionPointId;
		this.pensionPoint = pensionPoint;
		this.timeStamp = timeStamp;
	}


	

	public String getTimeStamp() {
		return timeStamp;
	}




	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}




	public PspfMemberModel() {
		super();
	}

	
	
	public int getPensionPointId() {
		return pensionPointId;
	}



	public void setPensionPointId(int pensionPointId) {
		this.pensionPointId = pensionPointId;
	}



	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPensionPoint() {
		return pensionPoint;
	}

	public void setPensionPoint(String pensionPoint) {
		this.pensionPoint = pensionPoint;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}
	
	
}
