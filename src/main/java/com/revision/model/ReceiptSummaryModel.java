package com.revision.model;

public class ReceiptSummaryModel {
	private String Org;
	private int count;
	private double amount;
	
	public ReceiptSummaryModel(String org, int count, double amount) {
		super();
		Org = org;
		this.count = count;
		this.amount = amount;
	}

	public ReceiptSummaryModel() {
		super();
	}

	public String getOrg() {
		return Org;
	}

	public void setOrg(String org) {
		Org = org;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
