package com.revision.model;

import java.util.List;

public class RevisionPensionerList {

	private List<RevisionPensionerModel> penList;

	public RevisionPensionerList(List<RevisionPensionerModel> penList) {
		super();
		this.penList = penList;
	}

	public RevisionPensionerList() {
		super();
	}

	public List<RevisionPensionerModel> getPenList() {
		return penList;
	}

	public void setPenList(List<RevisionPensionerModel> penList) {
		this.penList = penList;
	}
	
	
}
