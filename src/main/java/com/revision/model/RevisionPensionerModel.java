package com.revision.model;

public class RevisionPensionerModel {
	private String salaryScale;
	private String grade;
	private double basicSalary;
	private int retYear;
	private String type;
	
	

	public RevisionPensionerModel(String salaryScale, String grade, double basicSalary, int retYear, String type) {
		super();
		this.salaryScale = salaryScale;
		this.grade = grade;
		this.basicSalary = basicSalary;
		this.retYear = retYear;
		this.type = type;
	}

	public RevisionPensionerModel() {
		super();
	}

	public String getSalaryScale() {
		return salaryScale;
	}

	public void setSalaryScale(String salaryScale) {
		this.salaryScale = salaryScale;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}

	

	public int getRetYear() {
		return retYear;
	}

	public void setRetYear(int retYear) {
		this.retYear = retYear;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
