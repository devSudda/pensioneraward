package com.revision.model;

public class UserModel {
	private int userId;
	private String username;
	private String password;
	private String mobileNumber;
	private String role;
	private String name;
	private int pensionPoint;
	private int dsCode;
	
	public UserModel(int userId, String username, String password, String mobileNumber, String role, String name,
			int pensionPoint, int dsCode) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.mobileNumber = mobileNumber;
		this.role = role;
		this.name = name;
		this.pensionPoint = pensionPoint;
		this.dsCode = dsCode;
	}

	public UserModel() {
		super();
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPensionPoint() {
		return pensionPoint;
	}

	public void setPensionPoint(int pensionPoint) {
		this.pensionPoint = pensionPoint;
	}

	public int getDsCode() {
		return dsCode;
	}

	public void setDsCode(int dsCode) {
		this.dsCode = dsCode;
	}
	
}
