package com.revision.responce.model;

import java.util.List;

import com.revision.model.BasicDataDTO;

public class BasicDataList {
	private List<BasicDataDTO> dataList;

	public BasicDataList(List<BasicDataDTO> dataList) {
		super();
		this.dataList = dataList;
	}

	public BasicDataList() {
		super();
	}

	public List<BasicDataDTO> getDataList() {
		return dataList;
	}

	public void setDataList(List<BasicDataDTO> dataList) {
		this.dataList = dataList;
	}

}
