package com.revision.responce.model;

import java.util.List;

import com.revision.model.BasicPersonalInfoModel;
import com.revision.model.ContributionDetails;

public class ContributionResModel {
	private BasicPersonalInfoModel personal;
	private List<ContributionDetails> conDetails;
	
	public ContributionResModel(BasicPersonalInfoModel personal, List<ContributionDetails> conDetails) {
		super();
		this.personal = personal;
		this.conDetails = conDetails;
	}

	public ContributionResModel() {
		super();
	}

	public BasicPersonalInfoModel getPersonal() {
		return personal;
	}

	public void setPersonal(BasicPersonalInfoModel personal) {
		this.personal = personal;
	}

	public List<ContributionDetails> getConDetails() {
		return conDetails;
	}

	public void setConDetails(List<ContributionDetails> conDetails) {
		this.conDetails = conDetails;
	}
	
	
	
}
