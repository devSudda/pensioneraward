package com.revision.responce.model;

public class MilitoryBankData {
	private String Bankname;
	private String BankID;
	private String Branch;
	private String BranchID;
	private String AccNu;
	
	
	
	public MilitoryBankData(String bankname, String bankID, String branch, String branchID, String accNu) {
		super();
		Bankname = bankname;
		BankID = bankID;
		Branch = branch;
		BranchID = branchID;
		AccNu = accNu;
	}
	public MilitoryBankData() {
		// TODO Auto-generated constructor stub
	}
	public String getBankname() {
		return Bankname;
	}
	public void setBankname(String bankname) {
		Bankname = bankname;
	}
	public String getBankID() {
		return BankID;
	}
	public void setBankID(String bankID) {
		BankID = bankID;
	}
	public String getBranch() {
		return Branch;
	}
	public void setBranch(String branch) {
		Branch = branch;
	}
	public String getBranchID() {
		return BranchID;
	}
	public void setBranchID(String branchID) {
		BranchID = branchID;
	}
	public String getAccNu() {
		return AccNu;
	}
	public void setAccNu(String accNu) {
		AccNu = accNu;
	}
	
	
	
	
	
}
