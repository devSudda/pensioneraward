package com.revision.responce.model;

import org.springframework.stereotype.Component;


public class PensionerAward {

	private String name;
	private String status;
	private String resEmployee;
	private String date;
	private String nic;
	private String fileNum;
	private String solderNum;

	public PensionerAward() {
		super();
	}

	public PensionerAward(String name, String status, String resEmployee, String date, String nic, String fileNum,
			String solderNum) {
		super();
		this.name = name;
		this.status = status;
		this.resEmployee = resEmployee;
		this.date = date;
		this.nic = nic;
		this.fileNum = fileNum;
		this.solderNum = solderNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResEmployee() {
		return resEmployee;
	}

	public void setResEmployee(String resEmployee) {
		this.resEmployee = resEmployee;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public String getSolderNum() {
		return solderNum;
	}

	public void setSolderNum(String solderNum) {
		this.solderNum = solderNum;
	}

	@Override
	public String toString() {
		return "PensionerAward [name=" + name + ", status=" + status + ", resEmployee=" + resEmployee + ", date=" + date
				+ ", nic=" + nic + ", fileNum=" + fileNum + ", solderNum=" + solderNum + "]";
	}

}
