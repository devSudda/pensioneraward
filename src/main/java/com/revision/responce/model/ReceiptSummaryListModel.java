package com.revision.responce.model;

import java.util.List;

import com.revision.model.ReceiptSummaryModel;

public class ReceiptSummaryListModel {
	private List<ReceiptSummaryModel> recSum;

	public ReceiptSummaryListModel(List<ReceiptSummaryModel> recSum) {
		super();
		this.recSum = recSum;
	}

	public ReceiptSummaryListModel() {
		super();
	}

	public List<ReceiptSummaryModel> getRecSum() {
		return recSum;
	}

	public void setRecSum(List<ReceiptSummaryModel> recSum) {
		this.recSum = recSum;
	}
	
	
}
