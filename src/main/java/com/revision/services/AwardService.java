package com.revision.services;

import org.springframework.stereotype.Service;

import com.revision.data.DaoManager;
import com.revision.responce.model.PensionerAward;

@Service
public class AwardService {

	public PensionerAward getAward(String nic, Long filenum, String soldernum) {
		
		PensionerAward penAwd = new PensionerAward();
		penAwd = DaoManager.awarddao().getAward(nic, filenum, soldernum);
		return penAwd;
	}

}
