package com.revision.services;

import org.springframework.stereotype.Service;

import com.revision.data.DaoManager;
import com.revision.model.ContributionList;
import com.revision.responce.model.ReceiptSummaryListModel;

@Service
public class ContributionService {
	
	public ContributionList getContribution(int id) {
		ContributionList cbl=new ContributionList();
		cbl=DaoManager.contributtionDao().getContribution(id);
		return cbl;
	}
	
	public ReceiptSummaryListModel getReceipt(int month, int year) {
		ReceiptSummaryListModel rsm=new ReceiptSummaryListModel();
		rsm=DaoManager.contributtionDao().getReceiptSum(month, year);
		return rsm;
	}
}
