package com.revision.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.revision.DaoImpl.PspfMemberImpl;
import com.revision.data.DaoManager;
import com.revision.model.PspfMemberList;
import com.revision.model.PspfMemberModel;
import com.revision.responce.model.ContributionResModel;

@Service
public class PspfMemberService {
	
	public PspfMemberList getMembersList(int id) {
		List<PspfMemberModel> list=new ArrayList<PspfMemberModel>();
		list=new PspfMemberService().getMembers(id);
		
		return new PspfMemberList(list);
	}

	public List<PspfMemberModel> getMembers(int id){
		List<PspfMemberModel> list=new ArrayList<PspfMemberModel>();
		list=DaoManager.memberDao().getMember(id);
		
		return list;
	}
	
	public PspfMemberModel getMember(String nic, int pspfNo) {
		PspfMemberModel pspf=new PspfMemberModel();
		pspf=DaoManager.memberDao().getMemberByNic(nic, pspfNo);
		
		return pspf;
	}
	
	public List<String> getNumbers(){
		List<String> list=new ArrayList<String>();
		list=new PspfMemberImpl().getNuumbers();
		
		return list;
	}
	
	public ContributionResModel getConDetails(String nic) {
		ContributionResModel crm=new ContributionResModel();
		crm=new PspfMemberImpl().getConDetails(nic);
		return crm;
	}
}
