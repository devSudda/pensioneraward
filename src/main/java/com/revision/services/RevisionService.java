package com.revision.services;

import org.springframework.stereotype.Service;

import com.revision.data.DaoManager;
import com.revision.data.TokenManager;
import com.revision.exceptions.UnAuthorizedException;
import com.revision.model.RevisionPensionerList;
import com.revision.responce.model.BasicDataList;

@Service
public class RevisionService {

	public RevisionPensionerList getRevision(String nic, int penno, int pt) {
		RevisionPensionerList rpl=new RevisionPensionerList();
		rpl=DaoManager.revisionDao().getRevisionList(nic, penno, pt);
		return rpl;
	}
	
	public BasicDataList getBasiList(String nic, int penno, String token) throws UnAuthorizedException {
		
		BasicDataList bdl=new BasicDataList();
		try {
			
			bdl=DaoManager.revisionDao().getBasicData(nic, penno);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return bdl;
	}
}
