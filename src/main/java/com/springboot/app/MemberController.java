package com.springboot.app;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.revision.model.PaymentModel;
import com.revision.model.PspfMemberList;
import com.revision.model.PspfMemberModel;
import com.revision.responce.model.ContributionResModel;
import com.revision.services.PspfMemberService;

@RestController
@RequestMapping("/member")
@CrossOrigin(origins = "*")
public class MemberController {

	
	
	@GetMapping("/getMembers/{id}")
	public PspfMemberList getMemberList(@PathVariable int id) {
		System.out.println(id);
		
		PspfMemberList list=new PspfMemberList();
		
		
		list=new PspfMemberService().getMembersList(id);
		if(list==null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Member Not Found");
		}else {
		return list;
		}
	}
	
	@GetMapping("/getNumbers")
	public List<String> getNumbers(){
		List<String> list= new PspfMemberService().getNumbers();
		return list;
	}	
	
	@GetMapping("getMember/{nic}/{pspf}")
	public PspfMemberModel getMember(@PathVariable String nic, @PathVariable int pspf, HttpServletResponse response) {
		PspfMemberModel pspfModel=new PspfMemberModel();
		pspfModel=new PspfMemberService().getMember(nic, pspf);
		if(pspfModel.getId()==0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Member Not Found");
		}else {
		return pspfModel;
		}
	}
	
	@GetMapping("/getPayment/{nic}")
	public PaymentModel getPaymentDetails(@PathVariable String nic) {
		PaymentModel model=new PaymentModel();
		
		return model;
	}
	
	@GetMapping("/getContributionNic/{nic}")
	public ContributionResModel getConDetailsNic(@PathVariable String nic) {
		ContributionResModel cbr=new ContributionResModel();
		System.out.println("nic - "+nic);
		cbr=new PspfMemberService().getConDetails(nic);
		if(cbr.getPersonal().getPspfNo()==0) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY,"Contribution Not Found");
		}
		return cbr;
	}
}
