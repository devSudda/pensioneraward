package com.springboot.app;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.revision.exceptions.DataNotFoundException;
import com.revision.responce.model.MilitoryBankData;
import com.revision.services.MilitoryBankDetilList;

/**
 * Get Militory bank details- due to ( source api )error - SUDESH
 * @author Pension
 *
 */

@RestController
@RequestMapping("/bankDetails")
@CrossOrigin(origins = "*")
public class MilitoryBankDataController {
	
	@GetMapping("/bank/{nic}")
	public MilitoryBankData getBankData(@PathVariable("nic")String nic){
		
		MilitoryBankData md =  new MilitoryBankData();
		md = new MilitoryBankDetilList().getBkData(nic);
		
//		if(md.equals(null)) {
//			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "pensioner not found");
//		}
		
		if(md.getBankname()== null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		else {
			return md;
		}
		
 		
				
	}
	

	@GetMapping("/banks/{nic}")
	public MilitoryBankData getbankd(@PathVariable("nic")String nic) {
		
		return null;
	}

}

