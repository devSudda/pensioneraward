package com.springboot.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revision.responce.model.PensionerAward;
import com.revision.services.AwardService;

@RestController
@RequestMapping("/award")
public class PensionerAwardController {

	@GetMapping("/get/{nic}/{filenum}/{solnum}")
	public PensionerAward getAward(@PathVariable("nic") String nic, @PathVariable("filenum") Long fileNum,
			@PathVariable("solnum") String solnum) {

		PensionerAward pAward = new PensionerAward();
		AwardService awServe = new AwardService();
		pAward = awServe.getAward(nic, fileNum, solnum);
		return pAward;
	}

}
