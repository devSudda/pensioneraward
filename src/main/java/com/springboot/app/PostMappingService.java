package com.springboot.app;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
@Service
public class PostMappingService {

	private static List<Item>LoadItemsFromDb(){
		
	List<Item> itmObject= new ArrayList<Item>();
	
		itmObject.add(new Item(1,"item desc",100));
		itmObject.add(new Item(2,"item desc",200));
		itmObject.add(new Item(3,"item desc",200));
		return itmObject;
	}

	private List<Item> items = LoadItemsFromDb();
	
	List<Item> getAllItems(){
		return items;
	}

	public String addItems(Item item) {
		items.add(item);
		return"item added";
		
	}
	
}
