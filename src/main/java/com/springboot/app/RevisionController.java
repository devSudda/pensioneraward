package com.springboot.app;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.revision.data.TokenManager;
import com.revision.exceptions.UnAuthorizedException;
import com.revision.model.ContributionList;
import com.revision.model.RevisionPensionerList;
import com.revision.model.UserModel;
import com.revision.responce.model.BasicDataList;
import com.revision.responce.model.ReceiptSummaryListModel;
import com.revision.services.ContributionService;
import com.revision.services.RevisionService;

@RestController
@RequestMapping("/revision")
@CrossOrigin(origins = "*")
public class RevisionController {

	@GetMapping("/masterData/{nic}/{penno}")
	public RevisionPensionerList getContribution(@PathVariable("nic") String nic, @PathVariable("penno") int penno) {
		RevisionPensionerList rpl=new RevisionPensionerList();
		rpl=new RevisionService().getRevision(nic, penno, 0);
		return rpl; 
	}
	
	@GetMapping("/basic/{nic}/{penno}")
	public BasicDataList getBasicDataList(@PathVariable("nic") String nic, @PathVariable("penno") int penno, 
			@RequestHeader("Authorization") String token) throws UnAuthorizedException {
		String[] subToken=token.split(" ");
//		System.out.println(subToken[1]);
		TokenManager tm=new TokenManager();
		BasicDataList bdl=new BasicDataList();
		UserModel um=new UserModel();
		try {
			um=tm.isAuthorized(subToken[1]);
			bdl=new RevisionService().getBasiList(nic, penno,token);

			
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(um.getRole()==null) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,"Unauthorized..");
		}
		if(bdl.getDataList().size()==0){

			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Pensioner Not Found..");
		}else {
			return bdl;
		}
		
				
	}
}
