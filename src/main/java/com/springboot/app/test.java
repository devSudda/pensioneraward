package com.springboot.app;

import java.security.PublicKey;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class test {

	@Service
	public class CountService {
		public int count(int num1, int num2) {
	        return num1+num2;
	    }
	}
	 
	 @Autowired
	    private CountService countService;


	 @PostMapping("/add") 
	 public int getCount(@RequestBody Map<String, Integer> data) {

	     return countService.count(data.get("number1"), data.get("number2"));

	 }
	
	
	
	
	
//	@GetMapping("/test/{name}")
//		public String welcome(@PathVariable("name")String name) {
//		return  name + "chithen ";
//	}
//	
//	public String welcome(@RequestParam(name="name", defaultValue = "chithen") String name)
//	{
//		return "name "+ name;
//	}
	
	
	
	
	}
